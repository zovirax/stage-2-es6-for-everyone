import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    /*
    Please Attention
    Below there is lots of cr@p code, so it's better to brew coffee rather to read it
    But it works!
    */
    firstFighter.keys = [];
    secondFighter.keys = [];

    firstFighter.fullHealth = firstFighter.health;
    secondFighter.fullHealth = secondFighter.health;

    firstFighter.isCombo = true;
    secondFighter.isCombo = true;

    document.addEventListener('keydown', handleKeyDown({ firstFighter, secondFighter, resolve }));
    document.addEventListener('keyup', event => {
      const { code } = event;
      switch (code) {
        // The first fighter controls
        case controls.PlayerOneBlock:
          firstFighter.isBlock = false;
        case controls.PlayerOneAttack:
        case controls.PlayerOneCriticalHitCombination[0]:
        case controls.PlayerOneCriticalHitCombination[1]:
        case controls.PlayerOneCriticalHitCombination[2]:
          console.log('key 1-st released');
          firstFighter.keys = [];
          break;
        // The second fighter controls
        case controls.PlayerTwoBlock:
          secondFighter.isBlock = false;
        case controls.PlayerTwoAttack:
        case controls.PlayerTwoCriticalHitCombination[0]:
        case controls.PlayerTwoCriticalHitCombination[1]:
        case controls.PlayerTwoCriticalHitCombination[2]:
          console.log('key 2-nd released');
          secondFighter.keys = [];
          break;
      }
    });
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage >= 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return (fighter.attack || 0) * randomInteger(1, 2);
  const { attack = 0 } = fighter;
  const criticalHitChance = randomInteger(1, 2);
  const power = attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return (fighter.defense || 0) * randomInteger(1, 2);
  const { defense = 0 } = fighter;
  const dodgeChance = randomInteger(1, 2);
  const power = defense * dodgeChance;
  return power;
}

function handleKeyDown({ resolve, firstFighter, secondFighter }) {
  return event => {
    const fighter = savePressedKey({ firstFighter, secondFighter, event });
    if (!fighter) {
      return;
    }
    console.log(fighter);
    console.log(fighter.keys);
    const [firstKey] = fighter.keys.slice(-1);

    if (fighter === firstFighter) {
      if (isCombo(fighter.keys, controls.PlayerOneCriticalHitCombination) && fighter.isCombo) {
        firstFighter.attack *= 2;
        const damage = getDamage(firstFighter, secondFighter);
        secondFighter.health -= damage;
        const percentage = Math.ceil(100 * secondFighter.health / secondFighter.fullHealth);
        document.getElementById('right-fighter-indicator').style.width = `${percentage < 0 ? 0 : percentage}%`;
        console.warn(`First fighter hit critical combination, damaged ${damage} points`);
        firstFighter.attack /= 2;
        firstFighter.keys = [];
        fighter.isCombo = false;
        setTimeout(() => {
          console.log('1-st combo is available');
          fighter.isCombo = true;
        }, 10005);
      } else if (firstKey === controls.PlayerOneAttack) {
        if (secondFighter.keys.slice(-1)[0] === controls.PlayerTwoBlock) {
          console.warn('Second fighter blocked attack');
        } else {
          const damage = getDamage(firstFighter, secondFighter);
          secondFighter.health -= damage;
          const percentage = Math.ceil(100 * secondFighter.health / secondFighter.fullHealth);
          document.getElementById('right-fighter-indicator').style.width = `${percentage < 0 ? 0 : percentage}%`;
          console.warn(`First fighter damaged ${damage} points`);
        }
      }
    } else if (fighter === secondFighter) {
      if (isCombo(fighter.keys, controls.PlayerTwoCriticalHitCombination) && fighter.isCombo) {
        secondFighter.attack *= 2;
        const damage = getDamage(secondFighter, firstFighter);
        firstFighter.health -= damage;
        const percentage = Math.ceil(100 * firstFighter.health / firstFighter.fullHealth);
        document.getElementById('left-fighter-indicator').style.width = `${percentage < 0 ? 0 : percentage}%`;
        console.warn(`Second fighter hit critical combination, damaged ${damage} points`);
        secondFighter.attack /= 2;
        secondFighter.keys = [];
        fighter.isCombo = false;
        setTimeout(() => {
          console.log('2-nd combo is available');
          fighter.isCombo = true;
        }, 10005);
      } else if (firstKey === controls.PlayerTwoAttack) {
        if (firstFighter.keys.slice(-1)[0] === controls.PlayerOneBlock) {
          console.warn('First fighter blocked attack');
        } else {
          const damage = getDamage(secondFighter, firstFighter);
          firstFighter.health -= damage;
          const percentage = Math.ceil(100 * firstFighter.health / firstFighter.fullHealth);
          document.getElementById('left-fighter-indicator').style.width = `${percentage < 0 ? 0 : percentage}%`;
          console.warn(`Second fighter damaged ${damage} points`);
        }
      }
    }
    console.log('first:', firstFighter.health, 'second:', secondFighter.health);
    if (firstFighter.health <= 0) {
      resolve(secondFighter);
    } else if (secondFighter.health <= 0) {
      resolve(firstFighter);
    }
  };
}

function isCombo(keys = [], hitCombination = []) {
  keys = keys.slice(-3).filter((v, i, arr) => arr.indexOf(v) === i);
  return hitCombination.every(hit => {
    return keys.includes(hit);
  });
}

function savePressedKey({ firstFighter, secondFighter, event }) {
  const { code } = event;
  switch (code) {
    // The first fighter controls
    case controls.PlayerOneBlock:
      firstFighter.isBlock = true;
    case controls.PlayerOneAttack:
    case controls.PlayerOneCriticalHitCombination[0]:
    case controls.PlayerOneCriticalHitCombination[1]:
    case controls.PlayerOneCriticalHitCombination[2]:
      if (firstFighter.isBlock) {
        firstFighter.keys.push(code);
        return;
      }
      if (![...firstFighter.keys, code].slice(-3).filter((v, i, arr) => arr.indexOf(v) === i)
        .every(el => controls.PlayerOneCriticalHitCombination.includes(el))) {
        firstFighter.keys = [];
      }
      firstFighter.keys.push(code);
      return firstFighter;
    // The second fighter controls
    case controls.PlayerTwoBlock:
      secondFighter.isBlock = true;
    case controls.PlayerTwoAttack:
    case controls.PlayerTwoCriticalHitCombination[0]:
    case controls.PlayerTwoCriticalHitCombination[1]:
    case controls.PlayerTwoCriticalHitCombination[2]:
      if (secondFighter.isBlock) {
        secondFighter.keys.push(code);
        return;
      }
      if (![...secondFighter.keys, code].slice(-3).filter((v, i, arr) => arr.indexOf(v) === i)
        .every(el => controls.PlayerTwoCriticalHitCombination.includes(el))) {
        secondFighter.keys = [];
      }
      secondFighter.keys.push(code);
      return secondFighter;
    default:
      firstFighter.keys = [];
      secondFighter.keys = [];
  }
}

function randomInteger(min, max) {
  const rand = min + Math.random() * (max - min);
  return Math.round(rand);
}
