import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (!fighter) {
    return fighterElement;
  }

  const textLabels = createElement({ tagName: 'div' });

  Object.entries(fighter)
    .filter(([key]) => !['_id', 'source'].includes(key))
    .forEach(([key, value]) => {
      const textLabel = createElement({ tagName: 'span' });
      textLabel.innerText = `${key}: ${value}\n`;
      textLabels.append(textLabel);
    });

  fighterElement.append(createFighterImage(fighter));
  fighterElement.append(textLabels);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
