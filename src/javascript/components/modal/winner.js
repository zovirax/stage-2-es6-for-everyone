import { showModal } from './modal';

export function showWinnerModal(fighter) {
  showModal({ title: 'The Winner!', bodyElement: fighter.name });
}
