export const controls = {
  PlayerOneAttack: 'KeyA',
  PlayerOneBlock: 'KeyD',
  PlayerOneCriticalHitCombination: ['KeyQ', 'KeyW', 'KeyE'],

  PlayerTwoAttack: 'KeyJ',
  PlayerTwoBlock: 'KeyL',
  PlayerTwoCriticalHitCombination: ['KeyU', 'KeyI', 'KeyO']
}
